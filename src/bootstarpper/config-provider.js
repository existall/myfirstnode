var config = require('config');

const self = {};

self.get = function(name) {
 return config.get(name);
};

self.has = function(name){
    return config.has(name);
};

module.exports = self;