var Colu = require('colu');
var coluSettings = require('./colu-settings');
var coluProvider = require('../ryu/colu-provider');

var self = {};

self.init = function init(settings) {

    if (!settings) {
        settings = {
            network: coluSettings.network,
            privateSeed: coluSettings.privateSeed
        };
    }

    var colu = new Colu(settings);

    colu.on('connect', function () {

        coluProvider.setColu(colu);
        console.log("colu server is up");
    });

    colu.init();
};

module.exports = self;





