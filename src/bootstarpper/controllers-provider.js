var glob = require("glob");

const routers  = [];
const files = glob.sync('ryu/**/*-controller.js');

files.forEach(function(k,v){
    var router = require("../" + k.replace(".js", ""));
        routers.push(router);
});

module.exports = routers;
