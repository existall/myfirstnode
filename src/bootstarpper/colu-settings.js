const config = require('./config-provider');

const defaults = config.get("colu-settings");
var settings = {};

if(!defaults.has('network'))
    throw 'no network has been set';
else
    settings.network = defaults.get('network');

if(!defaults.has('privateSeed')){
    settings.privateSeed = process.env.COLU_PRIVATE_SEED;
}

if(!settings.privateSeed) {
    throw 'no colu private seed was provided';
}

module.exports = settings;