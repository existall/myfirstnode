var coluProvider = require('../colu-provider');
var _ = require('lodash');
var async = require('async');
var mapper = require('./assets-mapper');

var self = {};

self.getAssetsIds = function(callback){

    coluProvider.getColu().getAssets(function (err, addresses) {
        if(err) {
            callback(err);
            return;
        }

        var ids = _.map(addresses,function(item){
            return item.assetId;
        });

        callback(null,ids);
    });
};

self.issueAssets = function(assets, callback){

    var dtos = _.map(assets, function(asset){
        return mapper.mapToDto(asset);
    });

    async.reduce(dtos, [], function(memo, dto, cb){

        coluProvider.getColu().issueAsset(dto,function(err,body){
            if(err) {
                console.log('unable to issue asset name' + dto.metadata.assetName + '[' + err +']');
                cb(null,memo);
                return;
            }
            memo.push(body.assetId);
            cb(null,memo);
        });
    }, function(err, result){
        callback(null,result);
    });
};

module.exports = self;
