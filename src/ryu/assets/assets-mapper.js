var self = {};

self.mapToDto = function(source){

    return {
        amount: source.amount,
        divisibility: 0,
        reissueable: false,
        metadata: {
            'assetName': source.name
        }
    };
};

module.exports = self;