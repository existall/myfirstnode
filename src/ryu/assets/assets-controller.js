var assetsRepository = require('./assests-repository');

module.exports = function(router){
    router.get('/assets', function(req, res) {

        assetsRepository.getAssetsIds(function(err, ids){

            var lookup = {};
            var result = [];

            for (var i = 0; item = ids[i++];) {
                var name = item;

                if (!(name in lookup)) {
                    lookup[name] = 1;
                    result.push(name);
                }
            }

            res.json(result);
        })
    });

    router.put('/issue',function(req, res){

        assetsRepository.issueAssets(req.body.assets, function(err, ids){
            if(err)
                throw err;

            res.json(ids);
        });
    });
};