const self = {};

self.setColu = function(colu){
    if(!colu)
        throw 'colu object must be passed';
    self.colu = colu;
};

self.getColu = function() {
  if(!self.colu)
      throw 'No colu object was set';

    return self.colu;
};

module.exports = self;